import { Action, Command, Context, Wizard, WizardStep } from "nestjs-telegraf";
import { IWizardContext } from "./app.interface";
import { Inject } from "@nestjs/common";
import { VideoSpreadsheetService } from "./spreadsheet/spreadsheet.video.service";

const restartButton = {
    text: '🔄 Заново',
    callback_data: 'restart',
}

@Wizard('video')
export class ChooseVideoWizard {
    constructor(
        @Inject(VideoSpreadsheetService) private readonly spreadsheetService: VideoSpreadsheetService,
    ) {
    }

    resetState(@Context() ctx: IWizardContext) {
        ctx.wizard.state.data = {
            botPrevMsg: null,
            prevCursor: null,
            objectToSend: {
                type: 'video',
                link: '',
                category: 'Другое',
                universe: '',
            }
        };
    }

    async checkIfRerun(@Context() ctx: IWizardContext): Promise<boolean> {
        if (ctx.wizard.state.data.prevCursor === ctx.wizard.cursor) {
            await this.deleteLastUserMsg(ctx)
            return true;
        } else return false;
    }

    updateCursor(@Context() ctx: IWizardContext) {
        ctx.wizard.state.data.prevCursor = ctx.wizard.cursor;
    }

    async editPrevBotMsg(ctx: IWizardContext, content: string, options?: unknown) {
        try {
            await ctx.telegram.editMessageText(ctx.chat.id, ctx.wizard.state.data.botPrevMsg, undefined, content, options);
        } catch (e) {
            console.error(e.message);
        }
    }

    async deleteLastUserMsg(ctx: IWizardContext) {
        try {
            await ctx.telegram.deleteMessage(ctx.chat.id, ctx.message.message_id)
        } catch (e) {}
    }

    @Action('restart')
    @Command('start')
    async reset(@Context() ctx: IWizardContext) {
        const banlist = await this.spreadsheetService.getBanlist();
        if (banlist.includes(ctx.from.username)) return;
        this.resetState(ctx);
        ctx.wizard.selectStep(0)
        ctx.wizard.step(ctx)
        if (ctx.update.callback_query)
            await ctx.answerCbQuery();
    }

    // Ссылка
    @WizardStep(0)
    async step1Link(@Context() ctx: IWizardContext) {
        this.resetState(ctx);
        ctx.wizard.state.data.botPrevMsg = (await ctx.reply('Шалом! Это бот Саши Гольдштейна! Вижу, ты хочешь, чтобы Саша посмотрел какое-то видео. Пришли мне ссылку')).message_id;
        ctx.wizard.next();
    }

    // О какой Вселенной видео?
    @WizardStep(1)
    async step2(@Context() ctx: IWizardContext) {
        const regex = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.\S{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.\S{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.\S{2,}|www\.[a-zA-Z0-9]+\.\S{2,})/;
        const link = ctx.message.text;
        if (!regex.test(link)) {
            await this.deleteLastUserMsg(ctx);
            if (!await this.checkIfRerun(ctx)) {
                await this.editPrevBotMsg(ctx, 'Точно валидная ссылка? Попробуй еще раз, пожалуйста, не могу распознать :(', {
                    reply_markup: { inline_keyboard: [[restartButton]] },
                });
            }
            this.updateCursor(ctx);
            return
        }
        ctx.wizard.state.data.objectToSend.link = link
        await this.deleteLastUserMsg(ctx);
        await this.editPrevBotMsg(ctx, `Ссылка: <b>${link}</b>\n\n` + 'О какой Вселенной видео?', {
            parse_mode: 'HTML',
            reply_markup: {
                inline_keyboard: [
                    [restartButton]
                ],
            },
        });
        this.updateCursor(ctx);
        ctx.wizard.next();
    }

    // Категория
    @WizardStep(2)
    async step3(@Context() ctx: IWizardContext) {
        if (await this.checkIfRerun(ctx)) return;
        ctx.wizard.state.data.objectToSend.universe = ctx.message.text;
        const { link, universe } = ctx.wizard.state.data.objectToSend;
        await this.editPrevBotMsg(
            ctx,
            `Ссылка: <b>${link}</b>\n` +
            `Вселенная: ${universe}\n\n` +
            'Выбери категорию:', {
                parse_mode: 'HTML',
                reply_markup: {
                    inline_keyboard: [
                        [
                            { text: '🚑 Шизы и желтушники', callback_data: 'category:Шизы и желтушники' },
                            { text: '🗣️ Дискуссионный контент', callback_data: 'category:Дискуссионный контент' },
                        ],
                        [
                            { text: '💎 Качественное видео', callback_data: 'category:Качественное видео' },
                            { text: '🎨 Моё творчество', callback_data: 'category:Моё творчество' }
                        ],
                        [
                            { text: '🤷‍♂️ Другое', callback_data: 'category:Другое' },
                        ],
                        [restartButton]
                    ],
                },
            });
        this.updateCursor(ctx);
    }

    @Action(/category:.+/)
    async categoryButton(@Context() ctx: IWizardContext) {
        const [, answer] = ctx.callbackQuery.data.split(':')
        ctx.wizard.state.data.objectToSend.category = answer;
        await ctx.answerCbQuery()
        ctx.wizard.next();
        ctx.wizard.step(ctx);
    }

    // Проверка
    @WizardStep(3)
    async step4(@Context() ctx: IWizardContext) {
        if (await this.checkIfRerun(ctx)) return;
        const { link, universe, category } = ctx.wizard.state.data.objectToSend;
        await this.deleteLastUserMsg(ctx);
        await this.editPrevBotMsg(
            ctx,
            `Давай перепроверим:\n\n` +

            `Ссылка: <b>${link}</b>\n` +
            `Вселенная: ${universe}\n` +
            `Категория: <b>${category}</b>\n` +
            'Смотри, всё правильно?', {
                parse_mode: 'HTML',
                reply_markup: {
                    inline_keyboard: [
                        [
                            { text: '✅ Да, отправляй!', callback_data: 'confirm:yes' },
                            { text: '⛔ Нет, попробую заново', callback_data: 'confirm:no' }
                        ],
                    ],
                },
            });
        this.updateCursor(ctx)
    }

    @Action(/confirm:.+/)
    async confirmButton(@Context() ctx: IWizardContext) {
        const [, answer] = ctx.callbackQuery.data.split(':')
        await ctx.answerCbQuery();
        if (answer === 'no') {
            await this.reset(ctx);
        } else {
            ctx.wizard.next();
            this.updateCursor(ctx);
            ctx.wizard.step(ctx)
        }
    }

    // Концовка
    @WizardStep(4)
    async step5(@Context() ctx: IWizardContext) {
        try {
            await this.spreadsheetService.addVideo([{
                link: ctx.wizard.state.data.objectToSend.link,
                category: ctx.wizard.state.data.objectToSend.category,
                universe: ctx.wizard.state.data.objectToSend.universe,
                author: ctx.from.username,
                date: new Date().toLocaleString(),
            }], 'A:E');
        } catch (e) {
            await ctx.reply('Что-то пошло не так :( Попробуй ещё раз пожалуйста!');
            await this.reset(ctx);
            return;
        }

        await ctx.reply('Спасибо! Ты прекрасен, целую в пузико!\n\n' +
            'Если Саша посчитает твоё видео интересным — он посмотрит его на одном из своих стримов. Это бесплатно.\n\n' +
            'Если хочешь — можешь просто поддержать канал рублем подписавшись на наш Sponsr' +
            ' — https://sponsr.ru/stgeek/', {
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: '💰 Поддержать на Sponsr',
                            url: 'https://sponsr.ru/stgeek/',
                        },
                        {
                            text: '🔄 Отправить ещё видео',
                            callback_data: 'restart',
                        }
                    ]
                ]
            },
        });
    }
}
