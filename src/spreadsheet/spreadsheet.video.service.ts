import { Inject, Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";

import { SpreadsheetApi } from "./spreadsheet.api";
import { TVideosData } from "../app.interface";

@Injectable()
export class VideoSpreadsheetService extends SpreadsheetApi {
    private readonly _mainSheetName;
    private readonly _banListSheetName;

    constructor(
        @Inject(ConfigService) private readonly configService: ConfigService,
    ) {
        const spreadsheetId = configService.get('spreadsheet.id');
        const mainSheetName = configService.get('spreadsheet.sheetNames.video');
        const banlistSheetName = configService.get('spreadsheet.sheetNames.banlist');
        super(spreadsheetId)
        this._mainSheetName = mainSheetName;
        this._banListSheetName = banlistSheetName;
    }

    async getBanlist() {
        const banlist = await this._read(`${this._banListSheetName}!A2:A1000`);
        return banlist.flat();
    }

    async addVideo(rowsToProcess: TVideosData[], range: string) {
        const rowsToAdd = rowsToProcess.reduce<any>((acc, el) => {
            return [
                ...acc,
                [
                    'https://t.me/'+el.author,
                    el.link,
                    el.universe,
                    el.category,
                    el.date
                ],
            ];
        }, []);
        await this._appendRow(`${this._mainSheetName}!${range}`, rowsToAdd);
    }
}
