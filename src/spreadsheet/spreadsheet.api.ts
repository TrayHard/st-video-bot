import { Compute, GoogleAuth } from 'google-auth-library';
import { JSONClient } from 'google-auth-library/build/src/auth/googleauth';
import { google } from 'googleapis';

export class SpreadsheetApi {
    protected _auth: GoogleAuth<JSONClient>;
    protected _client: JSONClient | Compute;
    protected _sheets: any;
    protected _spreadsheetId: string;
    protected _ready = false;

    constructor(spreadsheetId: string) {
        this._auth = new google.auth.GoogleAuth({
            keyFile: './credentials.json',
            scopes: [
                'https://www.googleapis.com/auth/drive',
                'https://www.googleapis.com/auth/drive.file',
                'https://www.googleapis.com/auth/spreadsheets',
            ],
        });
        this._spreadsheetId = spreadsheetId;
        this.init();
    }

    async init() {
        this._client = await this._auth.getClient();
        this._sheets = google.sheets({ version: 'v4', auth: this._client });
        this._ready = true;
    }

    private async _run(cb: Function, timeout?: any) {
        timeout && clearTimeout(timeout);
        if (this._ready) return cb();
        else {
            const newTimeout = setTimeout(() => {
                return this._run(cb, newTimeout);
            }, 1000);
        }
    }

    protected async _appendRow(
        range: string,
        rowToAdd: string[][],
        options: null | object = null
    ) {
        this._run(async () => {
            try {
                await this._sheets.spreadsheets.values.append({
                    auth: this._auth,
                    spreadsheetId: this._spreadsheetId,
                    range,
                    valueInputOption: 'USER_ENTERED',
                    resource: {
                        values: rowToAdd,
                    },
                    ...(options || {}),
                });
            } catch (error) {
                console.error(error);
            }
        });
    }

    protected async _batchUpdateRows(
        sheetName: string,
        range: string,
        rowsToAdd: string[][],
        options: null | object = null
    ) {
        this._run(async () => {
            try {
                await this._sheets.spreadsheets.values.batchUpdate({
                    spreadsheetId: this._spreadsheetId,
                    resource: {
                        valueInputOption: 'USER_ENTERED',
                        data: [
                            {
                                range: `${sheetName}!${range}`,
                                majorDimension: 'ROWS',
                                values: rowsToAdd,
                            },
                        ],
                        includeValuesInResponse: false,
                    },
                });
            } catch (error) {
                console.error(error);
            }
        });
    }

    protected async _deleteRows(range: string, options: null | object = null) {
        this._run(async () => {
            try {
                await this._sheets.spreadsheets.values.clear({
                    auth: this._auth,
                    spreadsheetId: this._spreadsheetId,
                    range,
                });
            } catch (error) {
                console.error(error);
            }
        });
    }

    protected async _read(range: string, options: null | object = null) {
        let data = null;
        await this._run(async () => {
            try {
                const res = await this._sheets.spreadsheets.values.get({
                    auth: this._auth,
                    spreadsheetId: this._spreadsheetId,
                    range,
                });
                data = res.data.values
            } catch (error) {
                console.error(error);
            }
        });
        return data
    }

    protected async _updateRows(
        sheetName,
        rangeToDelete: string,
        rowToAdd: string[][]
    ) {
        this._run(async () => {
            try {
                await this._deleteRows(`${sheetName}!${rangeToDelete}`);
                setTimeout(async () => {
                    await this._appendRow(sheetName, rowToAdd);
                }, 1000);
            } catch (error) {
                console.error(error);
            }
        });
    }
}
