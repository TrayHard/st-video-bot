import { WizardContext } from "telegraf/typings/scenes";

export interface IWizardContext extends WizardContext {
    update: WizardContext['update'] & {
        callback_query?: object
    },
    wizard: WizardContext['wizard'] & {
        step: (ctx: IWizardContext) => void,
        state: {
            data: {
                botPrevMsg: number | null,
                prevCursor: number | null,
                objectToSend?: {
                    type: 'video',
                    link: string,
                    category: string,
                    universe: string,
                }
            }
        }
    },
    message: WizardContext['message'] & {
        text: string;
    }
}

export type TVideosData = {
    author: string,
    date: string,
    link: string;
    category: string;
    universe: string;
}
