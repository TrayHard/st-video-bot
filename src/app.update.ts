import { Command, InjectBot, Start, Update } from "nestjs-telegraf";
import { Context, Telegraf } from "telegraf";
import { Inject } from "@nestjs/common";
import { VideoSpreadsheetService } from "./spreadsheet/spreadsheet.video.service";

@Update()
export class AppUpdate {
    constructor(
        @InjectBot() private readonly bot: Telegraf<Context>,
        @Inject(VideoSpreadsheetService) private readonly spreadsheetService: VideoSpreadsheetService,
    ) {
        this.bot.telegram.setMyCommands([
            { command: 'start', description: 'Отправить видео Саше' },
        ]).catch(console.error);
    }

    @Start()
    async start(ctx) {
        await ctx.scene.leave();
        ctx.scene.enter('video');
    }

    @Command('test_test')
    async test() {
        await this.spreadsheetService.addVideo([{
            author: 'trayhard',
            link: 'https://www.youtube.com/watch?v=1',
            universe: 'SW',
            category: 'Другое',
            date: new Date().toLocaleString(),
        }], 'A:E');
    }
}
