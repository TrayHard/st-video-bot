import { Module } from '@nestjs/common';
import { TelegrafModule } from "nestjs-telegraf";
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as LocalSession from 'telegraf-session-local';

import { AppUpdate } from './app.update';
import { ChooseVideoWizard } from "./chooseVideo.wizard";
import config from './config';
import { VideoSpreadsheetService } from "./spreadsheet/spreadsheet.video.service";

const sessions = new LocalSession({ database: 'session_db.json' });

@Module({
    imports: [
        ConfigModule.forRoot({
            load: [config],
            isGlobal: true,
        }),
        TelegrafModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: (configService: ConfigService) => ({
                token: configService.get('token'),
                middlewares: [sessions.middleware()],
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [],
    providers: [ChooseVideoWizard, VideoSpreadsheetService, AppUpdate]
})
export class AppModule {
}
